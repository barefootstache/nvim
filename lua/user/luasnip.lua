local status_ok, ls = pcall(require, "luasnip")
if not status_ok then
	return
end

require("luasnip.loaders.from_lua").load({paths = {"~/.config/nvim/lua/user/snippets/"}})

ls.config.set_config({
  update_events = {"TextChanged", "TextChangedI"}, -- update changes as you type (when using function)
  enable_autosnippets = true,
  region_check_events = "CursorMoved",
  ext_opts = {
    [require("luasnip.util.types").choiceNode] = {
      active = {
        virt_text = {{ "o", "GruvboxOrange"}},
      },
    },
  },
})

--=== Key Maps ===--
-- Expanding
vim.keymap.set({"i","s"},"<a-p>", function()
  if ls.expand_or_jumpable() then
    ls.expand()
  end
end)

-- Jumping
vim.keymap.set({"i","s"},"<a-k>", function()
  if ls.jumpable(1) then
    ls.jump(1)
  end
end)
vim.keymap.set({"i","s"},"<a-j>", function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end)

-- Cycle through Choices
vim.keymap.set({"i","s"},"<a-l>", function()
  if ls.choice_active() then
    ls.change_choice(1)
  end
end)
vim.keymap.set({"i","s"},"<a-h>", function()
  if ls.choice_active() then
    ls.change_choice(-1)
  end
end)

-- shortcut to source my luasnips file again, which will reload my snippets
vim.keymap.set("n", "<CS-l>", "<cmd>source ~/.config/nvim/lua/user/luasnip.lua<CR>")


