local status_ok_mason, mason = pcall(require, "mason")
if not status_ok_mason then
  return
end

local status_ok_conf, mason_config = pcall(require, "mason-lspconfig")
if not status_ok_conf then
  return
end

mason.setup();
mason_config.setup();

require("user.lsp.handlers").setup()
require "user.lsp.configs"
require "user.lsp.null-ls"
