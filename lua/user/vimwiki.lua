local status_ok, vimwiki = pcall(require, "vimwiki")
if not status_ok then
  return
end

vimwiki.setup ({
  vimwiki_list = {path = '~/vimwiki/',
                  syntax = 'markdown',
                  ext = '.md'}
})
