local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
  return
end

local setup = {
  plugins = {
    marks = true, -- shows a list of your marks on ' and `
    registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    spelling = {
      enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
      suggestions = 20, -- how many suggestions should be shown in the list?
    },
    -- the presets plugin, adds help for a bunch of default keybindings in Neovim
    -- No actual key bindings are created
    presets = {
      operators = true, -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = true, -- adds help for motions
      text_objects = true, -- help for text objects triggered after entering an operator
      windows = true, -- default bindings on <c-w>
      nav = true, -- misc bindings to work with windows
      z = true, -- bindings for folds, spelling and others prefixed with z
      g = true, -- bindings for prefixed with g
    },
  },
  -- add operators that will trigger motion and text object completion
  -- to enable all native operators, set the preset / operators plugin above
  defer = function (ctx)
    --  ctx.operator == "gc" or ctx.operator == "Comments" or
    return ctx.mode == "V" or ctx.mode == "<C-V>"
  end,
  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and it's label
    group = "+", -- symbol prepended to a group
  },
  keys = {
    scroll_down = "<c-d>", -- binding to scroll down inside the popup
    scroll_up = "<c-u>", -- binding to scroll up inside the popup
  },
  win = {
    -- border = "rounded", -- none, single, double, shadow
    -- position = "bottom", -- bottom, top
    -- margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
    padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
    -- winblend = 0,
  },
  layout = {
    height = { min = 4, max = 25 }, -- min and max height of the columns
    width = { min = 20, max = 50 }, -- min and max width of the columns
    spacing = 3, -- spacing between columns
    align = "left", -- align columns left, center or right
  },
  show_help = true, -- show help message on the command line when the popup is visible
  triggers = {
    { "<auto>", mode = "nxsot" }, -- automatically setup triggers
    { "<leader>", mode = { "n", "v" } }
  },
}

local opts = {
  mode = "n", -- NORMAL mode
  prefix = "<leader>",
  buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true, -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true, -- use `nowait` when creating keymaps
}

local mappings = {
  mode = {"n","v"},
  {
    { "<leader>a", "<cmd>Alpha<cr>", desc = "Alpha" },
    { "<leader>b",
      "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",
      desc = "Buffers"
    },
    { "<leader>e", "<cmd>NvimTreeToggle<cr>", desc = "Explorer" },
    { "<leader>w", "<cmd>w!<cr>", desc = "Save", icon = { icon = "󰆓", color = "green" } },
    { "<leader>W", "<cmd>wa<cr>", desc = "Save All", icon = { icon = "󰆔", color = "green" } },
    { "<leader>q", "<cmd>q!<cr>", desc = "Quit" },
    { "<leader>Q", "<cmd>qa<cr>", desc = "Quit All" },
    { "<leader>c", "<cmd>Bdelete!<cr>", desc = "Close Buffer" },
    { "<leader>h", "<cmd>nohlsearch<cr>", desc = "No Highlight" },
    { "<leader>f",
      "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>",
      desc = "Find files",
    },
    { "<leader>F", "<cmd>Telescope live_grep theme=ivy<cr>", desc = "Find Text" },
    { "<leader>P", "<cmd>lua require('telescope').extensions.projects.projects()<cr>", desc = "Projects" },
  },
  {
    group = "Packer", icon = { icon = "󰏗", color = "orange" },
    { "<leader>pc", "<cmd>PackerCompile<cr>", desc = "Compile" },
    { "<leader>pi", "<cmd>PackerInstall<cr>", desc = "Install" },
    { "<leader>ps", "<cmd>PackerSync<cr>", desc = "Sync" },
    { "<leader>pS", "<cmd>PackerStatus<cr>", desc = "Status" },
    { "<leader>pu", "<cmd>PackerUpdate<cr>", desc = "Update" },
  },
  {
    group = "Git", icon = { icon = "󰊢", color = "orange" },
    { "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<cr>", desc = "Lazygit" },
    { "<leader>gj", "<cmd>lua require 'gitsigns'.next_hunk()<cr>", desc = "Next Hunk" },
    { "<leader>gk", "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", desc = "Prev Hunk" },
    { "<leader>gl", "<cmd>lua require 'gitsigns'.blame_line()<cr>", desc = "Blame" },
    { "<leader>gp", "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", desc = "Preview Hunk" },
    { "<leader>gr", "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", desc = "Reset Hunk" },
    { "<leader>gR", "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", desc = "Reset Buffer" },
    { "<leader>gs", "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", desc = "Stage Hunk" },
    { "<leader>gu",
      "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",
      desc = "Undo Stage Hunk",
    },
    { "<leader>go", "<cmd>Telescope git_status<cr>", desc = "Open changed file" },
    { "<leader>gb", "<cmd>Telescope git_branches<cr>", desc = "Checkout branch" },
    { "<leader>gc", "<cmd>Telescope git_commits<cr>", desc = "Checkout commit" },
    { "<leader>gd",
      "<cmd>Gitsigns diffthis HEAD<cr>",
      desc = "Diff",
    },
  },
  {
    group = "LSP",
    { "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>", desc = "Code Action" },
    { "<leader>ld",
      "<cmd>Telescope lsp_document_diagnostics<cr>",
      desc = "Document Diagnostics",
    },
    { "<leader>lw",
      "<cmd>Telescope lsp_workspace_diagnostics<cr>",
      desc = "Workspace Diagnostics",
    },
    { "<leader>lf", "<cmd>lua vim.lsp.buf.format{async=true}<cr>", desc = "Format" },
    { "<leader>li", "<cmd>LspInfo<cr>", desc = "Info" },
    { "<leader>lI", "<cmd>LspInstallInfo<cr>", desc = "Installer Info" },
    { "<leader>lj",
      "<cmd>lua vim.lsp.diagnostic.goto_next()<cr>",
      desc = "Next Diagnostic",
    },
    { "<leader>lk",
      "<cmd>lua vim.lsp.diagnostic.goto_prev()<cr>",
      desc = "Prev Diagnostic",
    },
    { "<leader>ll", "<cmd>lua vim.lsp.codelens.run()<cr>", desc = "CodeLens Action" },
    { "<leader>lq", "<cmd>lua vim.lsp.diagnostic.set_loclist()<cr>", desc = "Quickfix" },
    { "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<cr>", desc = "Rename" },
    { "<leader>ls", "<cmd>Telescope lsp_document_symbols<cr>", desc = "Document Symbols" },
    { "<leader>lS",
      "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
      desc = "Workspace Symbols",
    },
  },
  {
    group = "Search",
    { "<leader>sb", "<cmd>Telescope git_branches<cr>", desc = "Checkout branch" },
    { "<leader>sc", "<cmd>Telescope colorscheme<cr>", desc = "Colorscheme" },
    { "<leader>sh", "<cmd>Telescope help_tags<cr>", desc = "Find Help" },
    { "<leader>sM", "<cmd>Telescope man_pages<cr>", desc = "Man Pages" },
    { "<leader>sr", "<cmd>Telescope oldfiles<cr>", desc = "Open Recent File" },
    { "<leader>sR", "<cmd>Telescope registers<cr>", desc = "Registers" },
    { "<leader>sk", "<cmd>Telescope keymaps<cr>", desc = "Keymaps" },
    { "<leader>sC", "<cmd>Telescope commands<cr>", desc = "Commands" },
  },
  {
    group = "Terminal", icon = { icon = "", color = "green" },
    { "<leader>tn", "<cmd>lua _NODE_TOGGLE()<cr>", desc = "Node", icon = { icon = "󰎙", color = "yellow" } },
    { "<leader>tu", "<cmd>lua _NCDU_TOGGLE()<cr>", desc = "NCDU" },
    { "<leader>tt", "<cmd>lua _HTOP_TOGGLE()<cr>", desc = "Htop" },
    { "<leader>tp", "<cmd>lua _PYTHON_TOGGLE()<cr>", desc = "Python", icon = { icon = "", color = "green" } },
    { "<leader>tf", "<cmd>ToggleTerm direction=float<cr>", desc = "Float" },
    { "<leader>th", "<cmd>ToggleTerm size=10 direction=horizontal<cr>", desc = "Horizontal" },
    { "<leader>tv", "<cmd>ToggleTerm size=80 direction=vertical<cr>", desc = "Vertical" },
  },
}

which_key.setup(setup)
which_key.add(mappings, opts)
