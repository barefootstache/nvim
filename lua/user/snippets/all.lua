-- DOCS: https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md
local ls = require("luasnip")
local s = ls.s --> snippet
local i = ls.i --> insert node
local t = ls.t --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {}

local group = vim.api.nvim_create_augroup("All Snippets", { clear = true})
local file_pattern = "*.ts"

local upper_case = function(index)
    return f(function(args)
        local text = args[1][1]
        if #text > 1 then
            local capitalized_text = text:gsub("^%l", string.upper)
            return capitalized_text
        else
            return ""
        end
    end, {index})
end

ls.add_snippets("all", {
  s("todo", fmt("// TODO {}", { i(1, "something")})),
  s("pvConstructor", fmt("private {}: {}", { i(1, "myService"), upper_case(1) })),
  s("prvConstructor", fmt("private readonly {}: {}", { i(1, "myService"), upper_case(1) })),
  s("pbConstructor", fmt("public {}: {}", { i(1, "myService"), upper_case(1) })),
  s("logObj", fmt("console.log({{ {} }});", { i(1, "obj")})),
  s("descComment", fmt([[/**
 * {}
 */]], { i(1, "description") })),
  s("descCommentVer", fmt([[/**
 * {}
 * @version {}
 */]], { i(1, "description"), i(2, "version") })),
  s("descFkt", fmt([[/**
 * {}
 * @param {} - {}
 * @returns {}
 */]], { i(1, "description"), i(2, "param"), i(3, "paramDescription"), i(4, "returnDescription") })),
  s("descFktVer", fmt([[/**
 * {}
 * @version {}
 * @param {} - {}
 * @returns {}
 */]], { i(1, "description"), i(2, "version"), i(3, "param"), i(4, "paramDescription"), i(5, "returnDescription") })),
  s("descInput", fmt([[/**
 * {}
 * @defaultValue {}
 */]], { i(1, "description"), i(2, "value") })),
})
