local ls = require("luasnip")
local s = ls.s --> snippet
local i = ls.i --> insert node
local t = ls.t --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {}

local group = vim.api.nvim_create_augroup("Typescript Snippets", { clear = true})
local file_pattern = "*.vue"

ls.add_snippets("vue", {
  s("todo", {
    t("// TODO ")
  }),
  s("logObj", {
    t("console.log({"), i(1,"obj"), t("});")
  }),
  s("descComment", {
    t({"/**", " * "}), i(1, "description"), t({"", " */"})
  }),
  s("descCommentVer", {
    t({"/**", " * "}), i(1, "description"),
    t({"", " * @version "}), i(2, "version"),
    t({"", " */"})
  }),
  s("descFkt", {
    t({"/**", " * "}), i(1, "description"),
    t({"", " * @param "}), i(2, "param"), t(" - "), i(3, "paramDescription"),
    t({"", " * @returns "}), i(4, "returnDescription"),
    t({"", " */"})
  }),
  s("descFktVer", {
    t({"/**", " * "}), i(1, "description"),
    t({"", " * @version "}), i(2, "version"),
    t({"", " * @param "}), i(3, "param"), t(" - "), i(4, "paramDescription"),
    t({"", " * @returns "}), i(5, "returnDescription"),
    t({"", " */"})
  }),
  s("descInput", {
    t({"/**", " * "}), i(1, "description"),
    t({"", " * @defaultValue "}), i(2, "value"),
    t({"", " */"})
  })
})
