local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end


-- Setup mason configuration
require("mason").setup()
require "user.lsp.configs"
require("user.lsp.handlers").setup()

-- Setup neovim lua configuration
require('neodev').setup()

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
local servers = {
  -- clangd = {},
  -- gopls = {},
  -- pyright = {},
  -- rust_analyzer = {},
  -- tsserver = {},

  lua_ls = {
    Lua = {
      diagnostics = {
        globals = { 'vim' }
      },
      workspace = {
        library = vim.api.nvim_get_runtime_file("", true),
        checkThirdParty = false
      },
      telemetry = { enable = false },
    },
  },
}

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require('mason-lspconfig')

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    if server_name ~= nil then
      -- if server_name == "tsserver" then
      --   server_name = "ts_ls"
      -- end

      require('lspconfig')[server_name].setup {
        capabilities = capabilities,
        on_attach_kickstart = require("user.lsp.handlers").on_attach_kickstart,
        -- on_attach = require("user.lsp.handlers").on_attach,
        settings = servers[server_name],
      }
    end
  end,
}
