local status_ok, mason = pcall(require, "mason-lspconfig")
if not status_ok then
	return
end

local lspconfig = require("lspconfig")

local ensure_installed = { "jsonls", "typescript-language-server", "cssls" }
-- local ensure_installed = { "jsonls", "tsserver", "cssls" }

mason.setup({
	ensure_installed = ensure_installed,
})

for _, server in pairs(ensure_installed) do
  -- if server == "tsserver" then
  --   server = "ts_ls"
  -- end
	local opts = {
		on_attach = require("user.lsp.handlers").on_attach,
		capabilities = require("user.lsp.handlers").capabilities,
	}
	local has_custom_opts, server_custom_opts = pcall(require, "user.lsp.settings." .. server)
	if has_custom_opts then
		opts = vim.tbl_deep_extend("force", opts, server_custom_opts)
	end
	lspconfig[server].setup(opts)
end

